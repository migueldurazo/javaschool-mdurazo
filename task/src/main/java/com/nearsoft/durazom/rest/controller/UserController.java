package com.nearsoft.durazom.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.nearsoft.durazom.persistence.model.User;
import com.nearsoft.durazom.rest.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import com.nearsoft.durazom.service.UserService;

import javax.validation.Valid;
import java.sql.SQLException;

@RestController
@RequestMapping(path = "api/v1")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/users")
    public ResponseEntity<UserDto> createUser(
            @RequestBody @Valid UserDto userDto) throws SQLException {

        if (userDto == null)
            return ResponseEntity.noContent().build();

        User user = userService.create(userDto);

        userDto.setId(user.getId());

        return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);

    }

}
