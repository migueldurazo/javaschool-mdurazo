package com.nearsoft.durazom.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "{required.userDto.empty}")
    @Size(min = 3, message = "{size.userDto.lengthError}")
    private String name;


    @Email(message = "{valid.userDto.email}")
    @NotNull(message = "{required.userDto.empty}")
    @Size(min = 3, message = "{size.userDto.lengthError}")
    private String email;

    @NotNull(message = "{required.userDto.empty}")
    @NotEmpty(message = "{required.userDto.empty}")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
