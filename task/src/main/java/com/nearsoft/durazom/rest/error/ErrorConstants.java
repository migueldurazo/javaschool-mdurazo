package com.nearsoft.durazom.rest.error;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

public enum ErrorConstants {


    ERR_CONCURRENCY_FAILURE("error.concurrencyFailure"),
    ERR_ACCESS_DENIED ("error.accessDenied"),
    ERR_VALIDATION ("error.validation"),
    ERR_METHOD_NOT_SUPPORTED("error.methodNotSupported"),
    ERR_GENERAL("error.general"),
    ERR_INTERNAL_SERVER_ERROR("error.internalServerError");

    private static final String PROPERTIES_FILE = "ValidationMessages.properties";
    private final String message;
    private static boolean loaded = false;
    private static Properties validationMessagesProperties;

    public String getMessage() {

        if( !loaded ) {
            try {
                Resource resource = new ClassPathResource("/"+PROPERTIES_FILE);
                validationMessagesProperties = PropertiesLoaderUtils.loadProperties(resource);
            } catch (IOException e) {
                System.err.println("Could not read error messages properties file");
            } finally {
                loaded = true;
            }
        }
        System.out.println(validationMessagesProperties);

        if( validationMessagesProperties.isEmpty() || validationMessagesProperties.getProperty(message) == null ){
            return "Unknown Error";
        }else {
            return validationMessagesProperties.getProperty(message);
        }
    }

    ErrorConstants(String message ) {
        this.message = message;
    }

}