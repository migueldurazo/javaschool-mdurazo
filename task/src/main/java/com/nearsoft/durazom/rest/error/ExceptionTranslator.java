package com.nearsoft.durazom.rest.error;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ExceptionTranslator {

    private static final String defaultMessage = "Error has occured while processing this request";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorDTO processValidationError(MethodArgumentNotValidException ex) {

        ErrorDTO dto = new ErrorDTO(ErrorConstants.ERR_VALIDATION.getMessage());

        dto.setFieldErrors( convertToDTO( ex.getBindingResult().getFieldErrors()) );

        return dto;
    }

    private List<FieldErrorDTO> convertToDTO(List<FieldError> fieldErrors) {

        List<FieldErrorDTO> result = new ArrayList<>();

        for (FieldError fieldError : fieldErrors) {
            result.add(new FieldErrorDTO(fieldError.getObjectName(),
                                         fieldError.getField(),
                                         fieldError.getDefaultMessage()));
        }

        return result;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorDTO processPersistenceError(DataIntegrityViolationException ex) {

        return new ErrorDTO( ErrorConstants.ERR_GENERAL.getMessage() );

    }


}
