package com.nearsoft.durazom.service;

import com.nearsoft.durazom.rest.dto.UserDto;
import com.nearsoft.durazom.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.nearsoft.durazom.persistence.dao.UserRepository;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;

    }

    public User create(UserDto userDto) {

        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setPassword(encrypt(userDto.getPassword()));

        userRepository.save(user);

        return user;
    }

    String encrypt(String original) {

        return passwordEncoder.encode(original);

    }

    public boolean match(String original, String encrypted) {

        return passwordEncoder.matches(original, encrypted);

    }


}
