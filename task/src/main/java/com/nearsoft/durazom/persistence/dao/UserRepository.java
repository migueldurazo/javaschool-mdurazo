package com.nearsoft.durazom.persistence.dao;

import com.nearsoft.durazom.persistence.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
