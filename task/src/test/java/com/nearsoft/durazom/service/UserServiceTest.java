package com.nearsoft.durazom.service;

import com.nearsoft.durazom.Application;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import com.nearsoft.durazom.persistence.model.User;
import com.nearsoft.durazom.rest.dto.UserDto;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    private UserDto createUserDto(final String email) {
        final UserDto userDto = new UserDto();
        userDto.setEmail(email);
        userDto.setPassword("SecretPassword");
        userDto.setName("John");
        return userDto;
    }

    @Test
    public void givenUserDTO_whenSaveAndRetrieved_thenOK() {
        final String email = UUID.randomUUID().toString();
        final UserDto userDto = createUserDto(email);
        final User user = userService.create(userDto);
        assertNotNull(user);
        assertNotNull(user.getId());
        assertEquals(email, user.getEmail());
    }

    @Test
    public void givenSamePasswordTwice_whenEncryptedAndCompared_andNotSameString_thenOK() {
        final String passwordA = "example";
        final String passwordB = "example";
        final String encryptedPasswordA = userService.encrypt(passwordA);
        final String encryptedPasswordB = userService.encrypt(passwordB);
        assertNotEquals(encryptedPasswordA, encryptedPasswordB);
    }

    @Test
    public void givenPassword_whenEnctyptedAndMatched_thenOK() {
        final String original = "example";
        final String encryptedPassword = userService.encrypt(original);
        assertTrue(userService.match(original, encryptedPassword));
    }

    @Test(expected =  DataIntegrityViolationException.class )
    public void givenEmail_whenDuplicated_thenDataIntegrityViolationException() {
        final String email = UUID.randomUUID().toString();
        final UserDto userDtoA = createUserDto(email);
        final UserDto userDtoB = createUserDto(email);
        userService.create(userDtoA);
        userService.create(userDtoB);
    }
}